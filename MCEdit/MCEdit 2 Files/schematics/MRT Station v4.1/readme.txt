Minecart Rapid Transit Station v4.1 Final
By: Frumple
Released: April 8th, 2012
=========================================

A fully-automated minecart subway station designed for multiplayer servers.

===============================
Files included in this release:
===============================

Savefiles for Minecraft 1.2.5:
- A test world with 5 fully-functional stations
- 4 other savefiles showcasing the different types of stations, roofs, entrances, and tracks that are available as schematic files.

Schematic files created with MCEdit 0.1.0-10 unstable version:
- 8 station schematics for elevated, ground-level, subground-level, and underground usages, and in both north-south and east-west orientations.
- 8 roof schematics that are placed on top of the station to improve aesthetics and station identification.
- 10 entrance schematics used to provide walkways for passengers to enter/exit the station.
- 15 track schematics used to connect stations together.

=========
Features:
=========

----------------------
Minecart Arrival Bays:
----------------------

Fully Automated:
- Every incoming minecart comes to a stop in an arrival bay, then waits 9 seconds to allow the player to disembark, and then departs automatically without any player intervention.
Handles Multiplayer Traffic:
- 3 arrival bays per direction allow multiple players to arrive at nearly the same time without collisions. If all three bays are occupied, further incoming carts are looped around until an arrival bay becomes vacant.
Empty/Occupied Cart Detection:
- Carts leaving the arrival bay will continue out of the station if occupied, or be stored away into a dispenser if empty.
Foolproof:
- Carefully timed pistons ensure that players and carts are eventually pushed out of the bay so they do not interfere with other incoming arrivals. (* There is a bug in Minecraft 1.2.5 that currently prevents this from working, see bugs section below)
Smooth Ride:
- Arrival bay minecart tracks are kept as flat and straight as possible to improve the player's experience.
Audio Cues from Note Blocks:
- A single high-pitched tone is played when a cart enters an arrival bay.
- An increasing three-tone chime is played when a cart is about to leave an arrival bay.

-------------------------
Buttonless Departure Bay:
-------------------------

Instantaneous Departures:
- Just hop into the already waiting cart in the departure bay to begin your journey, without needing to push any buttons.
Self-replenishing:
- Whenever a cart leaves, another one will be called from the dispenser to take its place. If the cart in the departure bay is somehow missing, simply press the button nearby to request another cart from the dispenser.

------------------------------
Horizontal Minecart Dispenser:
------------------------------

High Capacity:
- Each dispenser can store up to 26 carts at a time.
Improved Cart Loading:
- New cart loader design prevents the player from loading too many carts into the dispenser at the same time, thus reducing the chance of a cart jam occuring.
Overflow Detection:
- If the dispenser is full, any further incoming carts are diverted to an overflow area located near the dispenser reload slot. The overflow area is a simple track that can hold an infinite number of carts.
Indicator Lights:
- 3 redstone lamps located near the cart loader report the status of the dispenser, including whether it is empty or full, or if there is dispenser flush in progress.

---------------
Other Features:
---------------

- Various maintenance access/emergency exit doors allow players to easily leave the mechanical areas of the station if they somehow get stuck inside.
- Colour coding for stations and tracks, so that rail lines can be easily identified by colour (example: "The Red Line")

===============
Specifications:
===============

Station Dimensions: 41W x 33L x 16H = 21648 blocks

MRT Station v4.1 is approximately 21.6% larger than v3.0.
(v3.0 Dimensions: 37W x 37L x 13H = 17791 blocks)

Total arrival bay waiting time: Approximately 9 seconds

Total capacity per dispenser: 26 carts
Total capacity per direction: 27 carts (26 in dispenser + 1 in departure bay)
Total capacity per station: 54 carts (27 carts x 2 dispenser per station)

Programs used to design and build this station:
- Minecraft 1.2.5
- MCEdit 0.1.0-10 unstable version
- Single Player Commands 3.2.2
- Mineconics.com (to design arched roofs)

==========================================
How to import the station into your world:
==========================================

Download MCEdit 0.10.010 unstable version from here: 
https://github.com/mcedit/mcedit/downloads

I highly recommend you use MCEdit to import these stations into your own worlds. For multiplayer worlds, you would need to import stations first into a single-player world using MCEdit, then copy that world into your server.
I have yet to test out importing this station using WorldEdit, but in previous versions of the station, it hasn't worked out well because WorldEdit had trouble importing track orientations and other things.

To import a station, follow these basic steps:

1. Select a station type: Elevated, Ground-level, Subground, or Underground.
2. Choose either a north-south or east-west orientation for the station. Each station schematic has a pair of gold blocks that are visible on the outside and just underneath the incoming and outgoing tracks. These gold blocks must point in either a south or west direction, as indicated by the station schematic filename.
3. Once you have a station type and orientation, find the corresponding station schematic file and use the import tool in MCEdit to copy it into your Minecraft world.
4. Add a roof schematic to the top of the station.
5. Attach entrances and corridors to the station as needed.
6. Connect your stations together using your own tracks or by using the track schematics provided.
7. Customize the colours and materials of your station by using the Replace tool in MCEdit. (Replace the Blue Wool blocks and/or the Iron blocks with any kind of solid block)
8. And you're done! Be sure to test out the station to make sure it's all working, and modify the text on any signs as needed.

======================
Bugs and Known Issues:
======================

- There is a bug in Minecraft 1.2.5 where pistons will extend through a player instead of pushing them. This means that the piston that normally pushes players out of the arrival bay will sometimes fail to do so.

===========================
Credits and Special Thanks:
===========================

- Renwallz for his improvements to my initial Horizontal Minecart Dispenser design:
http://renwallz.imgur.com/

- Sethbling for his Buttonless Minecart Launch Using Glass Panes (used for the departure bay):
http://www.youtube.com/watch?v=-YKgtrdw4iA

- KamiPrestige for his suggestion on adding a passenger drop to the arrival bay. Check out his channel here:
http://www.youtube.com/user/kamiprestige